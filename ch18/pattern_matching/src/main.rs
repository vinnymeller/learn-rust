#![feature(exclusive_range_pattern)]
fn if_let_to_match_one_case() {
    let favorite_color: Option<&str> = None;
    let is_tuesday = false;
    let age: Result<u8, _> = "34".parse();

    if let Some(color) = favorite_color {
        println!(
            "Using your favorite color, {}, as the background color",
            color
        );
    } else if is_tuesday {
        println!("Tuesday is a green day!");
    } else if let Ok(age) = age {
        if age > 30 {
            println!("Using purple as the background color");
        } else {
            println!("Using orange as the background color");
        }
    } else {
        println!("Using blue as the background color");
    }
}

fn while_let_conditional_loops() {
    let mut stack = Vec::new();
    stack.push(1);
    stack.push(2);
    stack.push(3);

    while let Some(top) = stack.pop() {
        println!("{}", top);
    }
}

fn for_loops() {
    let v = vec!['a', 'b', 'c', 'd'];
    for (index, value) in v.iter().enumerate() {
        println!("{} is at index {}", value, index);
    }
}

fn lets() {
    let (x, y, z) = (1, 2, 3);
    println!("{}, {}, {}", x, y, z);
}

fn function_params() {
    fn print_coordinates(&(x, y): &(i32, i32)) {
        println!("Current location: ({}, {})", x, y);
    }
    let point = (3, 5);
    print_coordinates(&point);
}

fn matching_literals() {
    let x = 1;
    match x {
        1 => println!("one"),
        2 => println!("two"),
        3 => println!("three"),
        _ => println!("anything"),
    }
}

fn matching_named_variables() {
    let x = Some(5);
    let y = 10;

    match x {
        Some(50) => println!("Got 50"),
        Some(y) => println!("Matched, y = {:?}", y),
        _ => println!("Default case, x = {:?}", x),
    }
}

fn matching_multiple_or_ranges() {
    let x = 1;
    match x {
        1 | 2 => println!("one or two lol"),
        _ => println!("other"),
    }

    let x = 5;
    match x {
        1..=5 => println!("one through five"),
        _ => println!("here"),
    }
}

fn deconstructing_structs() {
    struct Point {
        x: i32,
        y: i32,
    }
    let p = Point { x: 0, y: 7 };
    let Point { x: a, y: b } = p;
    assert_eq!(0, a);
    assert_eq!(7, b);
    let p = Point { x: 0, y: 7 };
    let Point { x: b, y: a } = p;
    assert_eq!(7, a);
    assert_eq!(0, b);

    match p {
        Point { x, y: 0 } => println!("On the x axis at {}", x),
        Point { x: 0, y } => println!("On the y axis at {}", y),
        Point { x, y } => println!("On neither axis: ({}, {})", x, y),
    }
}

fn destructuring_enums() {
    enum Message {
        Quit,
        Move { x: i32, y: i32 },
        Write(String),
        ChangeColor(i32, i32, i32),
    }

    let msg = Message::ChangeColor(0, 160, 255);
    match msg {
        Message::Quit => {
            println!("The Quit variant has no data to destructure");
        },
        Message::Move { x, y } => {
            println!("Move in the x direction {} and in the y direction {}", x, y);
        },
        Message::Write(text) => println!("Text message: {}", text),
        Message::ChangeColor(r, g, b) => {
            println!("Change the color to red {}, green {}, and blue {}", r, g, b);
        },
    }
}
fn nested_enums() {
    enum Color {
        Rgb(i32, i32, i32),
        Hsv(i32, i32, i32),
    }
    enum Message {
        Quit,
        ChangeColor(Color),
    }
    let msg = Message::ChangeColor(Color::Hsv(0, 160, 255));
    match msg {
        Message::Quit => { println!("Nothing to deconstruct!")},
        Message::ChangeColor(Color::Hsv(r,g,b)) => {
            println!("Hsv color");
        },
        Message::ChangeColor(Color::Rgb(r,g,b)) => {
            println!("Rgb color");
        }
    }
}

fn ignore_during_match() {
    let nums = (1,2,3);

    match nums {
        (1, _, _) => println!("First value is one!"),
        _ => println!("First value wasn't one"),
    }

    struct HDPoint {
        u: i32,
        v: i32,
        w: i32,
        x: i32,
        y: i32,
        z: i32,
    }
    let hdpoint = HDPoint {
        u: 1,
        v: 2,
        w: 3,
        x: 4,
        y: 5,
        z: 6,
    };

    match hdpoint {
        HDPoint { x: 4, .. } => println!("x is 4"), // i love this syntax
        _ => println!("x is not 4"),
    }
}

fn match_guards() {
    let num = Some(4);
    let y = 4;
    match num {
        Some(x) if x % 2 == 0 => println!("{} is even", x),
        Some(x) => println!("{} is odd", x),
        None => (),
    }
    match num {
        Some(50) => println!("Got 50"),
        Some(n) if n == y => println!("x matched y"),
        _ => println!("something else happened"),
    }
    enum Message {
        Hello { id: i32 },
    }
    let msg = Message::Hello { id: 5 };

    match msg {
        Message::Hello {
            id: id_val @ 1..=4,
        } => println!("id between 1 and 4 inclusive"),
        _ => println!("id is not between 1 and 4 inclusive"),
    }
}

fn main() {
    if_let_to_match_one_case();
    while_let_conditional_loops();
    for_loops();
    lets();
    function_params();
    matching_literals();
    matching_named_variables();
    matching_multiple_or_ranges();
    deconstructing_structs();
    destructuring_enums();
    nested_enums();
    ignore_during_match();
    match_guards();
}
