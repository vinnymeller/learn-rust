use std::fmt;
use std::io::Error;

type Kilometers = i32;
type thunk = Box<dyn Fn() + Send + 'static>;

fn custom_types() {
    let x: Kilometers = 5;
    let y = 5;
    let z: thunk = Box::new(|| println!("Hello"));
    println!("x + y = {}", x + y);
}

pub trait ExampleVerboseSyntax {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error>;
    fn flush(&mut self) -> Result<(), Error>;

    fn write_all(&mut self, buf: &[u8]) -> Result<(), Error>;
    fn write_fmt(&mut self, fmt: fmt::Arguments) -> Result<(), Error>;
}

type ResultCust<T> = std::result::Result<T, std::io::Error>;

pub trait LessVerbose {
    fn write(&mut self, buf: &[u8]) -> ResultCust<usize>;
    fn flush(&mut self) -> ResultCust<()>;
    // ...
}

// never return function
fn bar() -> ! {
    // ...
    loop {
        println!("forever and ever");
    }
}

fn dynamically_sized_types() {
    // has to be reference instead of str, because str is statically sized
    let s1: &str = "Hello there!";
    let s2: &str = "How's it going?";
}

// generics with unknown size at compile time

fn generic<T>(t: T) {
    // does something
}
// the above is implicitly the same as below
fn generic2<T: Sized>(t: T) {
    // does something
}
// if we want it to work with dynamically sized type, then below
fn generic3<T: ?Sized>(t: &T) {
    // does something
}

fn add_one(x: i32) -> i32 {
    x + 1
}

fn do_twice(f: fn(i32) -> i32, arg: i32) -> i32 {
    f(arg) + f(arg)
}

fn accept_closures_and_or_funcs() {
    let list_of_numbers = vec![1,2,3];
    let list_of_strings: Vec<String> = list_of_numbers.iter().map(|i| i.to_string()).collect();
    let list_of_strings_2: Vec<String> = list_of_numbers.iter().map(ToString::to_string).collect();
}

fn enum_initializers() {
    #[derive(Debug)]
    enum Status {
        Value(u32),
        Stop,
    }
    let list_of_statuses: Vec<Status> = (0u32..=20).map(Status::Value).collect();
    println!("{:?}", list_of_statuses);
}

fn returning_closures() -> Box<dyn Fn(i32) -> i32> {
    let closure = |x: i32| x + 1;
    Box::new(closure)
}

fn main() {
    custom_types();
    accept_closures_and_or_funcs();
    enum_initializers();
    let closure = returning_closures();
    println!("{:?}", closure(1));

}
