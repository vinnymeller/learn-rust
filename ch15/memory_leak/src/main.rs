use crate::List::{Cons, Nil};
use std::cell::RefCell;
use std::rc::{Rc, Weak};

#[derive(Debug)]
enum List {
    Cons(i32, RefCell<Rc<List>>),
    Nil,
}

impl List {
    fn tail(&self) -> Option<&RefCell<Rc<List>>> {
        match self {
            Cons(_, item) => Some(item),
            Nil => None,
        }
    }
}

fn screw_around_with_broken_list() {
    let a = Rc::new(Cons(5, RefCell::new(Rc::new(Nil))));

    println!("a initial rc count = {}", Rc::strong_count(&a));
    println!("a next item = {:?}", a.tail());
    let b = Rc::new(Cons(10, RefCell::new(Rc::clone(&a))));
    println!("a rc count after b creation = {}", Rc::strong_count(&a));
    println!("b initial rc count = {}", Rc::strong_count(&b));
    println!("b next item = {:?}", b.tail());

    if let Some(link) = a.tail() {
        *link.borrow_mut() = Rc::clone(&b);
    }

    println!("b rc count after changing a = {}", Rc::strong_count(&b));
    println!("a rc count after changing a = {}", Rc::strong_count(&a));

    // Uncomment the next line to see that we have a cycle;
    // it will overflow the stack
    //println!("a next item = {:?}", a.tail());
}

#[derive(Debug)]
struct Node {
    value: i32,
    children: RefCell<Vec<Rc<Node>>>,
}


impl Node {
    fn print_children_ref_counts(&self) {
        for child in self.children.borrow().iter() {
            println!("in node with value {}", self.value);
            println!("{}", Rc::strong_count(child));
            child.print_children_ref_counts();
        }
    }
}

fn screw_around_with_node() {
    let leaf = Rc::new(Node {
        value: 3,
        children: RefCell::new(vec![]),
    });

    let branch = Rc::new(Node {
        value: 5,
        children: RefCell::new(vec![Rc::clone(&leaf)]),
    });

    let deeper_branch = Rc::new(Node {
        value: 7,
        children: RefCell::new(vec![Rc::clone(&leaf), Rc::clone(&branch)]),
    });

    deeper_branch.print_children_ref_counts();
}

#[derive(Debug)]
struct SelfAwareNode {
    value: i32,
    parent: RefCell<Weak<SelfAwareNode>>,
    children: RefCell<Vec<Rc<SelfAwareNode>>>,
}


fn screw_around_with_bigger_node() {
    let leaf = Rc::new(SelfAwareNode {
        value: 1,
        parent: RefCell::new(Weak::new()),
        children: RefCell::new(vec![]),
    });
    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
    let branch = Rc::new(SelfAwareNode {
        value: 2,
        parent: RefCell::new(Weak::new()),
        children: RefCell::new(vec![Rc::clone(&leaf)]),
    });
    *leaf.parent.borrow_mut() = Rc::downgrade(&branch);
    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
}





fn main() {
    screw_around_with_broken_list();
    screw_around_with_node();
    screw_around_with_bigger_node();
}
