#![feature(test)]
extern crate test;
use std::cell::RefCell;
use std::ops::Deref;
use std::rc::Rc;

#[derive(Debug)]
enum List {
    Cons(Rc<RefCell<i32>>, Rc<List>),
    Nil,
}

struct MyBox<T>(T);

impl<T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

struct CustomSmartPointer {
    data: String,
}

impl Drop for CustomSmartPointer {
    fn drop(&mut self) {
        println!("Dropping CustomSmartPointer with data `{}`!", self.data);
    }
}

use crate::List::{Cons, Nil};
fn main() {
    let value = Rc::new(RefCell::new(5));
    let value2 = RefCell::new(5);
    {
        {
            {
                let mut mutref = value.borrow_mut();
                println!("{}", mutref);
                *mutref += 1;
                println!("{}", mutref);
            }
            let mut mutref = value.borrow_mut();
            println!("{}", mutref);
            *mutref += 1;
            println!("{}", mutref);
        }
        let mut mutref = value.borrow_mut();
        println!("{}", mutref);
        *mutref += 1;
        println!("{}", mutref);
    }
    let imutref = value.borrow();
}

fn alloc_box() -> Box<String> {
    let mut wholestr = Box::new(String::new());
    for i in 0..10_000 {
        let char_to_push = std::char::from_u32(i % 255).expect("Invalid char");
        wholestr.push(char_to_push);
    }
    wholestr
}

fn alloc_reg() -> String {
    let mut wholestr = String::new();
    for i in 0..10_000 {
        let char_to_push = std::char::from_u32(i % 255).expect("Invalid char");
        wholestr.push(char_to_push);
    }
    wholestr
}

#[cfg(test)]
mod benchmarks {

    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_alloc_box(b: &mut Bencher) {
        b.iter(|| {
            alloc_box();
        });
    }

    #[bench]
    fn bench_alloc_normal(b: &mut Bencher) {
        b.iter(|| {
            alloc_reg();
        });
    }
}
