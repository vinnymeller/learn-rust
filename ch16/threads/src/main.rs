use std::rc::Rc;
use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

fn play_with_basic_threads() {
    let handle = thread::spawn(|| {
        // closure being sent to thread
        for i in 1..10 {
            println!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    let handle2 = thread::spawn(|| {
        // closure being sent to thread
        for i in 1..10 {
            println!("hi number {} from the second spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });
    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }

    handle.join().unwrap();
    handle2.join().unwrap();

    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }

    let v = vec![1, 2, 3];

    let handle = thread::spawn(move || {
        // move keyword tells compiler to move ownership of v to the spawned thread
        println!("Here's a vector: {:?}", v);
    });
    handle.join().unwrap();
}

fn play_with_channels() {
    let (tx, rx) = mpsc::channel();
    let tx1 = tx.clone();
    let tx2 = tx.clone();

    let vals = vec![
        String::from("hi"),
        String::from("from"),
        String::from("the"),
        String::from("main"),
        String::from("thread"),
    ];
    let vals2 = vals.clone();
    let vals3 = vals.clone();

    thread::spawn(move || {
        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });
    thread::spawn(move || {
        for val in vals2 {
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });
    thread::spawn(move || {
        for val in vals3 {
            tx2.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}

fn play_with_mutexes() {
    let m = Mutex::new(5);
    {
        let mut num = m.lock().unwrap();
        *num = 6;
    }
    println!("m = {:?}", m);

    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        handles.push(handle);
    }
    for handle in handles {
        handle.join().unwrap();
    }
    println!("Result: {}", *counter.lock().unwrap());
}

fn make_a_deadlock() {
    let m = Arc::new(Mutex::new(5));
    let handle = thread::spawn(move || {
        let m = Arc::clone(&m);
        let m2 = Arc::clone(&m);
        let mut num = m.lock().unwrap();
        *num = 6;

        let handle2 = thread::spawn(move || {
            let m = Arc::clone(&m2);
            let mut num = m.lock().unwrap();
            *num = 7;
        });
        handle2.join().unwrap();
    });
    handle.join().unwrap();
}


fn main() {
    //    play_with_basic_threads();
    //    play_with_channels();
    //play_with_mutexes();
    //make_a_deadlock(); // success. i actually fked up making a deadlock a couple times. but it works(?) now
    println!("To avoid deadlock, comment it out");
}
